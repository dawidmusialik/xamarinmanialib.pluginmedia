#  XamarinManiaLib.PluginMedia

Przygotował Dawid Musialik.

Celem stworzenia tej aplikacji było napisanie biblioteki, która będzie wykorzystywana w aplikacjach mojego autorstwa. Ma ona za zadanie ułatwić pracę z pluginem MediaPlugin (https://github.com/jamesmontemagno/MediaPlugin).

### Instalacja
Należy dodać odwołanie do projektu “XamarinManiaLib.PluginMedia”. Oraz dyrektywy w klasach, które będą używały biblioteki: 

     using XamarinManiaLib.PluginMedia;

Należy również zainstalować plugin MediaPlugin, ACR.UserDialogs.

### Użycie
##### Tworzenie obiektu:
    public Photo Image = new Photo("", "iconAdd.png");
    ---
    Image.SetImageButtonHeightAndWidth(30, 30);
    Image.SetImageHeightAndWidth(300, 300);
    Image.SetImageQuality(50, 2000, XamarinManiaLib.PluginMedia.Model.PhotoSizeType.MaxWidthHeight);
    
##### Umieszczenie na widoku:
Sposób 1:

    Image.GetView();
W widoku zostanie umieszczony przycisk do wybrania/wykonania zdjęcia, jeżeli akcja zostanie pozytywnie zakończona, ikona zamieni się na obraz.

Sposób 2:

    Image.GetViewImage();
W widoku zostanie umieszczony obraz.

Sposób 3:

    Image.GetViewImageButton();
W widoku zostanie umieszczona ikona do pwybrania/wykonania obrazu. 

##### Ustawienie obrazu:
    Image.SetImageSource(object _ImageSource);
Metoda przyjmuje takie obiekty jak:
* string,
* ImageSource
* StreamImageSource

##### Pobranie obrazu:
    Image.GetPhotoByteAsString();;
Metoda zwraca serializowany obiekt Json (byte[])


##### Sprawdzanie obrazu:
    Image.IsNewPhoto();
Metoda zwraca wartość true/false w zależności czy zostało wykonane/pobrane nowe zdjęcie;

    Image.IsDeletedPhoto();
Metoda zwraca wartość true/false w zależności czy zdjęcie zostało usunięte;