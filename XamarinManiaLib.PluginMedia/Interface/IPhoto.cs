﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using XamarinManiaLib.PluginMedia.Model;

namespace XamarinManiaLib.PluginMedia.Interface
{
    internal interface IPhoto
    {
        /// <summary>
        /// Ustawienie zdjęcia które zostanie wykonane lub wybrane
        /// </summary>
        /// <param name="ImageSource">Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        void SetImageSource(object ImageSource);

        /// <summary>
        /// Ustawienie zdjęcia pomocniczego w roli przycisku
        /// </summary>
        /// <param name="ImageSource">>Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        void SetImageButtonSource(object ImageButtonSource);

        /// <summary>
        /// Ustawienie jakości zdjęcia
        /// </summary>
        /// <param name="CompressQuality">Kompresja zdjęcia: '0' - Maksymalna kompresja, najgorsza jakość / '100' - Brak kompresji, najlepsza jakość</param>
        /// <param name="CustomPhotoSize">
        /// Wielkość zdjęciaj, '100' - Maksymalna wielkość / '99', '98' - Wielkość pomniejszana procentowo względem maksymalnej wielkości.
        /// LUb wielkość zdjęcia maksymalna szerokość i wysokość. Zależy od parametru SizeType.
        /// </param>
        /// <param name="SizeType">
        /// Przyjmuje dwie wartości:
        /// MaxWidthHeight - odpowiada maksymalnej szerokości i wysokości zdjęcia
        /// PercentResize - odpowiada procentowemu pomniejszeniu zdjęcia</param>
        void SetImageQuality(int CompressQuality, int CustomPhotoSize, PhotoSizeType SizeType);

        /// <summary>
        /// Metoda zwracająca widok zdjęcia i pomocnicznego przycisku do jego wykonania
        /// </summary>
        /// <returns></returns>
        StackLayout GetView();
    }
}
