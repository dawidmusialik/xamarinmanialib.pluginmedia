﻿using System;
using XamarinManiaLib.PluginMedia.Model;
using Xamarin.Forms;
using XamarinManiaLib.PluginMedia.UserActions;
using System.Threading.Tasks;
using XamarinManiaLib.PluginMedia.Interface;
using Plugin.Media.Abstractions;
using System.Runtime.InteropServices;

namespace XamarinManiaLib.PluginMedia
{
    public class Photo: PhotoModel, IPhoto
    {
        private int _CompressQuality, _PhotoSize;
        private PhotoSize _PhotoSizeType;

        private bool _IsPhotoDeleted;
        public Photo()
        {
            SetItem();
        }
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="_ImageSource">Ustawienie źródła zdjęcia. Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        /// <param name="ImageButtonSource">UStawienie źródła zdjęcia pomocnicznego (przycisk wybierania). Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        public Photo(object _ImageSource, object ImageButtonSource)
        {
            SetItem(_ImageSource, ImageButtonSource);
        }

        /// <summary>
        /// Ustawienie źródła obrazu
        /// </summary>
        /// <param name="ImageSource">Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        public void SetImageSource(object _ImageSource) //Dopisać jesli nie null
        {
            var status = SetItemImages(ImageMain, _ImageSource);
            if (status) 
            {
                if(_ImageSource.GetType() == typeof(string))
                {
                    if(!string.IsNullOrEmpty((string)_ImageSource))
                    {
                        SetImageVisible(true);
                    }
                    else
                    {
                        SetImageVisible(false);
                    }
                }
                else
                {
                    SetImageVisible(true);
                }
            }
            else
            {
                SetImageVisible(false);
            }
        }

        /// <summary>
        /// Ustawienie źródła obrazu wybierania
        /// </summary>
        /// <param name="ImageSource">Przyjmuje wartość 'string' lub 'ImageSource'</param>
        public void SetImageButtonSource(object ImageButtonSource)
        {
            SetItemImagesButton(ImageButton, ImageButtonSource);
        }

        /// <summary>
        /// Ustawienie jakości zdjęcia
        /// </summary>
        /// <param name="CompressQuality">Kompresja zdjęcia: '0' - Maksymalna kompresja, najgorsza jakość / '100' - Brak kompresji, najlepsza jakość</param>
        /// <param name="CustomPhotoSize">
        /// Wielkość zdjęciaj, '100' - Maksymalna wielkość / '99', '98' - Wielkość pomniejszana procentowo względem maksymalnej wielkości.
        /// LUb wielkość zdjęcia maksymalna szerokość i wysokość. Zależy od parametru SizeType.
        /// </param>
        /// <param name="SizeType">
        /// Przyjmuje dwie wartości:
        /// MaxWidthHeight - odpowiada maksymalnej szerokości i wysokości zdjęcia
        /// PercentResize - odpowiada procentowemu pomniejszeniu zdjęcia</param>
        public void SetImageQuality(int CompressQuality = 50, int PhotoSize = 3000, PhotoSizeType SizeType = PhotoSizeType.MaxWidthHeight)
        {
            _CompressQuality = CompressQuality;
            _PhotoSize = PhotoSize;

            switch (SizeType)
            {
                case PhotoSizeType.MaxWidthHeight:
                    {
                        _PhotoSizeType = Plugin.Media.Abstractions.PhotoSize.MaxWidthHeight;
                        break;
                    }
                case PhotoSizeType.PercentResize:
                    {
                        _PhotoSizeType = Plugin.Media.Abstractions.PhotoSize.Custom;
                        break;
                    }
            }
        }
        
        /// <summary>
        /// Ustawienie wysokości i szerekości zdjęcia
        /// </summary>
        /// <param name="Height">Wysokość zdjęcia, jeżeli Height == 0, nie jest brane pod uwagę</param>
        /// <param name="Width">Szerokość zdjęcia, jeżeli Width == 0, nie jest brane pod uwagę</param>
        public void SetImageHeightAndWidth(int Height = 0, int Width = 0)
        {
            if(Height != 0)
            {
                ImageMain.HeightRequest = Height;
            }
            if(Width != 0)
            {
                ImageMain.WidthRequest = Width;
            }
        }

        /// <summary>
        /// Ustawienie wysokości i szerekości zdjęcia pomocniczego
        /// </summary>
        /// <param name="Height">Wysokość zdjęcia, jeżeli Height == 0, nie jest brane pod uwagę</param>
        /// <param name="Width">Szerokość zdjęcia, jeżeli Width == 0, nie jest brane pod uwagę</param>
        public void SetImageButtonHeightAndWidth(int Height = 0, int Width = 0)
        {
            if (Height != 0)
            {
                ImageButton.HeightRequest = Height;
            }
            if (Width != 0)
            {
                ImageButton.WidthRequest = Width;
            }
        }

        /// <summary>
        /// Pobranie widoku
        /// </summary>
        /// <returns></returns>
        public StackLayout GetView()
        {
            return StackLayout;
        }
        public Image GetViewImage()
        {
            return ImageMain;
        }
        public Image GetViewImageButton()
        {
            return ImageButton;
        }

        public bool IsImageTapped()
        {
            return ImageService.IsImagePickOrTake;
        }


        /// <summary>
        /// Pobranie zdjęcia byte[] jako string
        /// </summary>
        /// <returns></returns>
        public string GetPhotoByteAsString()
        {
            if (ImageService.IsPhotoExist()) return Newtonsoft.Json.JsonConvert.SerializeObject(ImageService.GetPhotoByte());
            else return "";
        }
        
        /// <summary>
        /// Metoda zwracająca informację czy zostało wykonane lub wybrane nowe zdjęcie
        /// </summary>
        /// <returns></returns>
        public bool IsNewPhoto()
        {
            return ImageService.IsNewPhoto();
        }

        /// <summary>
        /// Metoda zwracająca informację czy zdjęcie zostało usunięte
        /// </summary>
        /// <returns></returns>
        public bool IsDeletedPhoto()
        {
            return _IsPhotoDeleted;
        }

        /// <summary>
        /// Ustawienie elementów
        /// </summary>
        /// <param name="ImageSource">Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        /// <param name="ImageButtonSource">Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        private void SetItem(object _ImageSource = null, object ImageButtonSource = null)
        {
            bool statusImage = false, statusImageButton = false;
            _IsPhotoDeleted = false;
           

            //Ustawienie źródła Image, oraz jego statusu czy istnieje
            if (_ImageSource != null) statusImage = SetItemImages(ImageMain, _ImageSource);
            else statusImage = false;

            //Ustawienie źródła ImageButton, oraz jego statusu czy istnieje
            if (ImageButtonSource != null)
            {
                statusImageButton = SetItemImagesButton(ImageButton, ImageButtonSource);
            }

            else statusImageButton = false;

            //Ustawienie widoczności Image oraz ImageButton
            if (statusImage) SetImageVisible(true); //Jeżeli Image istnieje, jest widoczne, a ImageButton nie jest.
            else SetImageVisible(false); //Image button nie istnieje i jest nie widoczne, a ImageButton jest jest.

            SetImageDefaultQuality();
            SetItemOnStackLayout();
            SetImageTapGesture();
            SetImageButtonTapGesutre();
        }

        /// <summary>
        /// Ustawienie podstawowej jakości wybranego i wykonanego zdjęcia
        /// </summary>
        private void SetImageDefaultQuality()
        {
            _CompressQuality = 0;
            _PhotoSize = 100;
            _PhotoSizeType = PhotoSize.Custom;
        }

        /// <summary>
        /// Ustawienie elementów na widoku
        /// </summary>
        private void SetItemOnStackLayout()
        {
            StackLayout.Children.Add(ImageMain);
            StackLayout.Children.Add(ImageButton);
        }

        /// <summary>
        /// Jeżeli Image jest widoczne, to ImageButton nie jest i na odwrót
        /// </summary>
        private void SetImageVisible(bool ImageVisibility)
        {
            ImageMain.IsVisible = ImageVisibility;
            ImageButton.IsVisible = !ImageVisibility;
        }

        /// <summary>
        /// Ustawienie źródel obiektów typu Image przekazanych w parametrze.
        /// Jeżeli parametr jest inny niż docelowy rzuca Exception
        /// </summary>
        /// <param name="ImagesObject">Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        /// <param name="ImageSource">Przyjmuje wartość 'string', 'ImageSource' lub 'StreamImageSource'</param>
        /// <returns></returns>
        private bool SetItemImages(Image ImagesObject, object _ImageSource)
        {
            if (_ImageSource != null)
            {
                if (_ImageSource.GetType() == typeof(string))
                {
                    if (!string.IsNullOrEmpty((string)_ImageSource))
                    {
                        ImagesObject.Source = (string)_ImageSource;
                        return true;
                    }
                    else
                    {
                       return false;
                    }
                }
                else if (_ImageSource.GetType() == typeof(ImageSource))
                {
                    ImagesObject.Source = (Xamarin.Forms.ImageSource)_ImageSource;
                    return true;
                }
                else if (_ImageSource.GetType() == typeof(StreamImageSource))
                {
                    ImagesObject.Source = (Xamarin.Forms.StreamImageSource)_ImageSource;
                    return true;
                }
                else
                {
                    throw new Exception("Nie rozpoznano typu obiektu: " + nameof(_ImageSource));
                }
            }
            else return false;
        }
        private bool SetItemImagesButton(Image ImagesObject, object _ImageSource)
        {
            if (_ImageSource != null)
            {
                if (_ImageSource.GetType() == typeof(string))
                {
                    if (!string.IsNullOrEmpty((string)_ImageSource))
                    {
                        ImagesObject.Source = (string)_ImageSource;
                        return true;
                    }
                    else
                    {
                        ImageButton.Source = StreamImageSource.FromFile("IconAdd.png");
                        return true;
                    }
                }
                else if (_ImageSource.GetType() == typeof(ImageSource))
                {
                    ImagesObject.Source = (Xamarin.Forms.ImageSource)_ImageSource;
                    return true;
                }
                else if (_ImageSource.GetType() == typeof(StreamImageSource))
                {
                    ImagesObject.Source = (Xamarin.Forms.StreamImageSource)_ImageSource;
                    return true;
                }
                else
                {
                    throw new Exception("Nie rozpoznano typu obiektu: " + nameof(_ImageSource));
                }
            }
            else return false;
        }

        /// <summary>
        /// Ustawienie akcji kliknięcia na zdjęcie
        /// </summary>
        private void SetImageTapGesture()
        {
            ImageTapGesture.Command = new Command(async () =>
            {
                switch (_PhotoSizeType)
                {
                    case PhotoSize.MaxWidthHeight:
                        {
                            ImageActions _ImageActions = new ImageActions(ImageService, ActionNewImage(), ActionDeleteImage(), Convert.ToInt32(_CompressQuality), Convert.ToInt32(_PhotoSize));
                            await _ImageActions.Invoke();
                            break;
                        }
                    case PhotoSize.Custom:
                        {
                            ImageActions _ImageActions = new ImageActions(ImageService, ActionNewImage(), ActionDeleteImage(), Convert.ToInt16(_CompressQuality), Convert.ToInt16(_PhotoSize));
                            await _ImageActions.Invoke();
                            break;
                        }
                }
            });

            ImageMain.GestureRecognizers.Add(ImageTapGesture);
        }

        /// <summary>
        /// Ustawienie akcji kliknięcia na ikonę dodawania zdjęcia
        /// </summary>
        private void SetImageButtonTapGesutre()
        {
            ImageButtonTapGesture.Command = new Command(async () =>
            {
                switch (_PhotoSizeType)
                {
                    case PhotoSize.MaxWidthHeight:
                        {
                            ImageButtonActions _ImageButtonActions = new ImageButtonActions(ImageService, ActionNewImage(), Convert.ToInt32(_CompressQuality), Convert.ToInt32(_PhotoSize));
                            await _ImageButtonActions.Invoke();
                            break;
                        }
                    case PhotoSize.Custom:
                        {
                            ImageButtonActions _ImageButtonActions = new ImageButtonActions(ImageService, ActionNewImage(), Convert.ToInt16(_CompressQuality), Convert.ToInt16(_PhotoSize));
                            await _ImageButtonActions.Invoke();
                            break;
                        }
                }
            });
            ImageButton.GestureRecognizers.Add(ImageButtonTapGesture);
        }

        /// <summary>
        /// Akcja wywoływana w momencie pobrania lub zrobienia nowego zdjęcia
        /// </summary>
        /// <returns></returns>
        private Action ActionNewImage()
        {
            var _Action = new Action(() =>
            {
                if (ImageService.IsPhotoExist())
                {
                    SetImageVisible(true); //Image - Visibile , ImageButton - No Visible
                    SetImageSource(ImageService.GetPhotoPath() as string);
                    _IsPhotoDeleted = false;
                }
            });
            return _Action;
        }

        /// <summary>
        /// Akcja wywoływana w momencie kasowania zdjęcia
        /// </summary>
        /// <returns></returns>
        private Action ActionDeleteImage()
        {
            var _Action = new Action(() =>
            {
                if (!ImageService.IsPhotoExist())
                {
                    SetImageVisible(false); //Image - No Visibile , ImageButton - Visible
                    SetImageSource("");
                    _IsPhotoDeleted = true;
                }
            });
            return _Action;
        }
    }
}
