﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Styl", "IDE0044:Dodaj modyfikator tylko do odczytu", Justification = "<Oczekujące>", Scope = "member", Target = "~F:XamarinManiaLib.PluginMedia.UserActions.ImageActions._PhotoSizeType")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Styl", "IDE0044:Dodaj modyfikator tylko do odczytu", Justification = "<Oczekujące>", Scope = "member", Target = "~F:XamarinManiaLib.PluginMedia.UserActions.ImageButtonActions._PhotoSizeType")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Styl", "IDE0044:Ustaw pole jako tylko do odczytu", Justification = "<Oczekujące>", Scope = "member", Target = "~F:XamarinManiaLib.PluginMedia.UserActions.ImageActions._CompressQuality")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Styl", "IDE0044:Ustaw pole jako tylko do odczytu", Justification = "<Oczekujące>", Scope = "member", Target = "~F:XamarinManiaLib.PluginMedia.UserActions.ImageActions._PhotoSize")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Styl", "IDE0044:Ustaw pole jako tylko do odczytu", Justification = "<Oczekujące>", Scope = "member", Target = "~F:XamarinManiaLib.PluginMedia.UserActions.ImageButtonActions._CompressQuality")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Styl", "IDE0044:Ustaw pole jako tylko do odczytu", Justification = "<Oczekujące>", Scope = "member", Target = "~F:XamarinManiaLib.PluginMedia.UserActions.ImageButtonActions._PhotoSize")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Styl", "IDE0017:Uprość inicjowanie obiektów", Justification = "<Oczekujące>", Scope = "member", Target = "~M:XamarinManiaLib.PluginMedia.Services.PhotoService.PickPhoto(System.Int32,System.Int32,Plugin.Media.Abstractions.PhotoSize)~System.Threading.Tasks.Task{System.Boolean}")]

