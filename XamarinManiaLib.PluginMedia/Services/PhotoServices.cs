﻿using Acr.UserDialogs;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using XamarinManiaLib.Logger.Instance;
using XamarinManiaLib.Logger.Model.Enum;

namespace XamarinManiaLib.PluginMedia.Services
{
    internal class PhotoService
    {
        private MediaFile photo;
       // private bool checkPhone = false;
        private bool isPhotoExist = false;

        private bool photoTaken = false;
        private bool photoPick = false;
        public bool IsImagePickOrTake = false;


        /// <summary>
        /// Konstruktor
        /// </summary>
        public PhotoService()
        {
            CrossMedia.Current.Initialize();
            isPhotoExist = false;
        }

        /// <summary>
        /// Pobranie lokalizacji pliku z urządzenia.
        /// </summary>
        /// <returns>Zwraca ciąg tekstowy lokalizacji pliku.</returns>
        public string GetPhotoPath()
        {
            if (photo != null)
            {
                return photo.Path;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Usuwanie zdjęcia jeśli istnieje.
        /// </summary>
        /// <returns>Zwracany wynik prawda jeśli usunięto, fałsz jeśli nie usunięto lub wystąpił błąd.</returns>
        public async Task<bool> DeletePhoto()
        {
            bool _tmp = false;
            try
            {
                if (isPhotoExist == true)
                {
                    if (photoTaken == true)
                    {
                        if (File.Exists(photo.AlbumPath) == true)
                        {
                            File.Delete(photo.AlbumPath);
                            _tmp = true;
                        }
                        if (File.Exists(photo.Path))
                        {
                            File.Delete(photo.Path);
                            _tmp = true;
                        }
                    }

                    if (photoPick == true)
                    {
                        if (File.Exists(photo.Path))
                        {
                            File.Delete(photo.Path);
                            _tmp = true;
                        }
                    }

                    photoPick = false;
                    photoTaken = false;
                    isPhotoExist = false;
                }
                else
                {
                    _tmp = false;
                }
            }
            catch(Exception ex)
            {
                var Error = ex.Message;
                _tmp = false;
            }
            finally
            {

            }
            return _tmp;

        }

        /// <summary>
        /// Pobieranie tablicy bajtów zdjęcia
        /// </summary>
        /// <returns></returns>
        public byte[] GetPhotoByte()
        {
            byte[] _tmp = null;
            MemoryStream _memory;
            if (isPhotoExist == true)
            {
                _memory = new MemoryStream();
                photo.GetStream().CopyTo(_memory);
                return _memory.ToArray();
            }
            else
            {
                _tmp = null;
            }
            return _tmp;
            
        }

        /// <summary>
        /// Metoda zwraca informację czy zostało wykonane lub wybrane nowe zdjęcie
        /// </summary>
        /// <returns>bool</returns>
       public bool IsNewPhoto()
        {
            if (photoTaken == true || photoPick == true)
            {
                return true;
            }
            else return false;
        }


        /// <summary>
        /// Metoda pozwalająca wykonać zdjęcie za pomocą aparatu urządzenia
        /// </summary>
        /// <param name="CompressQuality">Kompresja zdjęcia: '0' - Maksymalna kompresja, najgorsza jakość / '100' - Brak kompresji, najlepsza jakość</param>
        /// <param name="_PhotoSize">Jeżeli PhotoSize.Custom: Wielkość zdjęciaj, '100' - Maksymalna wielkość / '99', '98' - Wielkość pomniejszana procentowo względem maksymalnej wielkości.
        /// Lub PhotoSize.MaxWidthHeight - Należy ustawić pożądaną maksymalną szerokość i wysokość zdjęcia</param>
        /// <param name="_PhotoSizeEnum">Należy wybrać:
        /// PhotoSize.MaxWidthHeight - Aby ustawić maksymalną szerokość i wysokość zdjęcia
        /// lub PhotoSize.Custom - Aby ustawić procentową wielkośc zdjęcia</param>
        /// <returns></returns>
        public async Task<bool> TakePhoto(int _CompressQuality, int _PhotoSize, PhotoSize _PhotoSizeEnum = PhotoSize.Custom)
        {
            try
            {
                IsImagePickOrTake = true;
                if (CrossMedia.Current.IsCameraAvailable == true && CrossMedia.Current.IsTakePhotoSupported == true && isPhotoExist == false)
                {
                    var takePhotoOptions = new Plugin.Media.Abstractions.StoreCameraMediaOptions
                    {
                        Directory = "CyberPies",
                        AllowCropping = true,
                        SaveToAlbum = true,
                        DefaultCamera = CameraDevice.Rear,
                    };

                    takePhotoOptions.CompressionQuality = _CompressQuality;

                    switch (_PhotoSizeEnum)
                    {
                        case PhotoSize.MaxWidthHeight:
                            {
                                takePhotoOptions.PhotoSize = PhotoSize.MaxWidthHeight;
                                takePhotoOptions.MaxWidthHeight = _PhotoSize;
                                break;
                            }
                        case PhotoSize.Custom:
                            {
                                takePhotoOptions.PhotoSize = PhotoSize.Custom;
                                takePhotoOptions.CustomPhotoSize = _PhotoSize;
                                break;
                            }
                    }

                    photo = await CrossMedia.Current.TakePhotoAsync(takePhotoOptions);
                }
                else
                {
                    if(!CrossMedia.Current.IsCameraAvailable)
                    {
                        UserDialogs.Instance.Alert("Kamera jest niedostępna, nie można wykonać zdjęcia.");
                        return false;
                    }
                    else if (!CrossMedia.Current.IsTakePhotoSupported)
                    {
                        UserDialogs.Instance.Alert("Urządzenia nie wspiera wykonywania zdjęć.");
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                IsImagePickOrTake = false;

                LoggerInstance.Instance.Log(LogType.File, LogLevel.Error, "Błąd podczas wykonywania zdjęcia: " + "Wiadomość: " + ex.Message + "Szczegóły: " + ex.StackTrace);
                var error = ex.Message;
            }
            finally
            {
                IsImagePickOrTake = false;

                if (photo != null)
                {
                    isPhotoExist = true;
                    photoTaken = true;
                    photoPick = false;
                }
                else
                {
                    isPhotoExist = false;
                    photoTaken = false;
                    photoPick = false;
                }
                //var t1 = photo.GetStream();
                //var t2 = photo.AlbumPath; //Lokalizacja pliku w katalogu Zdjęcia
                //var t3 = photo.GetStreamWithImageRotatedForExternalStorage();
                //var t4 = photo.Path; //Lokalizacja pliku w katalogu aplikacji
            }

            if (isPhotoExist) return true;
            else return false;
        }

        /// <summary>
        /// Sprawdzenia czy zdjęcie zostało wykonane lub pobrane z urządzenia
        /// </summary>
        /// <returns>bool</returns>
        public bool IsPhotoExist()
        {
            return isPhotoExist;
        }

        /// <summary>
        /// W przypadku przymusowej modyfikacji zmiennej
        /// </summary>
        /// <param name="IsExists"></param>
        public void IsPhotoExistChangeManual(bool IsExists)
        {
            isPhotoExist = IsExists;

            if(!IsExists)
            {
                photoPick = false;
                photoTaken = false;
                isPhotoExist = false;
                photo = null;
            }
        }

        /// <summary>
        /// Metoda pozwalająca wybrać zdjęcie z urządzenia
        /// </summary>
        /// <param name="CompressQuality">Kompresja zdjęcia: '0' - Maksymalna kompresja, najgorsza jakość / '100' - Brak kompresji, najlepsza jakość</param>
        /// <param name="_PhotoSize">Jeżeli PhotoSize.Custom: Wielkość zdjęciaj, '100' - Maksymalna wielkość / '99', '98' - Wielkość pomniejszana procentowo względem maksymalnej wielkości.
        /// Lub PhotoSize.MaxWidthHeight - Należy ustawić pożądaną maksymalną szerokość i wysokość zdjęcia</param>
        /// <param name="_PhotoSizeEnum">Należy wybrać:
        /// PhotoSize.MaxWidthHeight - Aby ustawić maksymalną szerokość i wysokość zdjęcia
        /// lub PhotoSize.Custom - Aby ustawić procentową wielkośc zdjęcia</param>
        /// <returns></returns>
        public async Task<bool> PickPhoto(int CompressQuality, int _PhotoSize, PhotoSize _PhotoSizeEnum = PhotoSize.Custom)
        {
            try
            {
                IsImagePickOrTake = true;

                var PickMediaOptions = new PickMediaOptions();

                PickMediaOptions.CompressionQuality = CompressQuality;

                switch (_PhotoSizeEnum)
                {
                    case PhotoSize.MaxWidthHeight:
                        {
                            PickMediaOptions.PhotoSize = PhotoSize.MaxWidthHeight;
                            PickMediaOptions.MaxWidthHeight = _PhotoSize;
                            break;
                        }
                    case PhotoSize.Custom:
                        {
                            PickMediaOptions.PhotoSize = PhotoSize.Custom;
                            PickMediaOptions.CustomPhotoSize = _PhotoSize;
                            break;
                        }
                }

                photo = await CrossMedia.Current.PickPhotoAsync(PickMediaOptions);
                //photo = await CrossMedia.Current.PickPhotoAsync();
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                IsImagePickOrTake = false;
            }
            finally
            {
                IsImagePickOrTake = false;

                if (photo != null)
                {
                    isPhotoExist = true;
                    photoPick = true;
                    photoTaken = false;
                }
                else
                {
                    isPhotoExist = false;
                    photoPick = false;
                    photoTaken = false;
                }
                //var t1 = photo.GetStream();
                //var t2 = photo.AlbumPath;
                //var t3 = photo.GetStreamWithImageRotatedForExternalStorage();
                //var t4 = photo.Path; //Lokalizacja pliku w katalogu aplikacji - Temporary
            }

            if (isPhotoExist) return true;
            else return false;
        }

    }
}