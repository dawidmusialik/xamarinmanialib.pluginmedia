﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinManiaLib.PluginMedia.Model
{
    public enum PhotoSizeType
    {
        /// <summary>
        /// Maksymalna szerokość i wysokość zdjęcia
        /// </summary>
        MaxWidthHeight = 0,

        /// <summary>
        /// Procentowa wielkość zdjęcia
        /// </summary>
        PercentResize = 1
    }
}
