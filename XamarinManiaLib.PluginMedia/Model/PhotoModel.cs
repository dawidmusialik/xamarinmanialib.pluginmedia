﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Xamarin.Forms;
using XamarinManiaLib.PluginMedia.Services;

namespace XamarinManiaLib.PluginMedia.Model
{
    public class PhotoModel
    {
        /// <summary>
        /// </summary>
        /// Główne zdjęcie
        public Image ImageMain { get; set; }
        /// <summary>
        /// Kliknięcie na główne zdjęcie
        /// </summary>
        protected TapGestureRecognizer ImageTapGesture;

        /// <summary>
        /// Zdjęcie pomocnicze, dodawanie głównego zdjęcia
        /// </summary>
        public Image ImageButton { get; set; }
        /// <summary>
        /// Kliknięcie na pomocnicze zdjęcie
        /// </summary>
        protected TapGestureRecognizer ImageButtonTapGesture;

        /// <summary>
        /// Serwis obsługujący 
        /// </summary>
        internal PhotoService ImageService { get; set; }
        /// <summary>
        /// Zmienna widoku, która przechowuje zdjęcie i zdjęcie pomocnicze
        /// </summary>
        public StackLayout StackLayout { get; set; }


        /// <summary>
        /// Konstruktor
        /// </summary>
        public PhotoModel()
        {
            CreateItem();
            SetItem();
        }

        /// <summary>
        /// Tworzenie elementów
        /// </summary>
        private void CreateItem()
        {
            ImageMain = new Image();
            ImageTapGesture = new TapGestureRecognizer();
            ImageButton = new Image();
            ImageButtonTapGesture = new TapGestureRecognizer();
            ImageService = new PhotoService();
            StackLayout = new StackLayout();
        }

        /// <summary>
        /// Ustawienie położenia elementów na widoku
        /// </summary>
        private void SetItem()
        {
            ImageMain.HorizontalOptions = LayoutOptions.Center;
            ImageMain.VerticalOptions = LayoutOptions.Center;

            ImageButton.HorizontalOptions = LayoutOptions.Center;
            ImageButton.VerticalOptions = LayoutOptions.Center;

            StackLayout.HorizontalOptions = LayoutOptions.Center;
            StackLayout.VerticalOptions = LayoutOptions.Center;
        }
    }
}