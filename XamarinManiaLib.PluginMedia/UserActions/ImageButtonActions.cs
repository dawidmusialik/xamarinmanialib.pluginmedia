﻿using Acr.UserDialogs;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XamarinManiaLib.PluginMedia.Services;

namespace XamarinManiaLib.PluginMedia.UserActions
{
    internal class ImageButtonActions
    {
        private List<ActionSheetOption> ListActionSheetOptions;
        private ActionSheetConfig ActionSheetConfig;
        private int _CompressQuality, _PhotoSize;
        private PhotoSize _PhotoSizeType;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <param name="PercentCompresion">Kompresja zdjęcia: '0' - Maksymalna kompresja, najgorsza jakość / '100' - Brak kompresji, najlepsza jakość</param>
        /// <param name="PercentPhotoSize">Wielkość zdjęciaj, '100' - Maksymalna wielkość / '99', '98' - Wielkość pomniejszana procentowo względem maksymalnej wielkości.
        public ImageButtonActions(PhotoService _PhotoService, Action NewImage, Int16 PercentCompresion, Int16 PercentPhotoSize)
        {
            _CompressQuality = PercentCompresion;
            _PhotoSize = PercentPhotoSize;
            _PhotoSizeType = PhotoSize.Custom;

            CreateItem();
            SetItem(_PhotoService, NewImage);
        }
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <param name="PercentCompresion">Kompresja zdjęcia: '0' - Maksymalna kompresja, najgorsza jakość / '100' - Brak kompresji, najlepsza jakość</param>
        /// <param name="PhotoMaxWidthHeight">Maksymalna szerokość i wysokość zdjęcia</param>
        public ImageButtonActions(PhotoService _PhotoService, Action NewImage, Int32 PercentCompresion, Int32 PhotoMaxWidthHeight)
        {
            _CompressQuality = PercentCompresion;
            _PhotoSize = PhotoMaxWidthHeight;
            _PhotoSizeType = PhotoSize.MaxWidthHeight;

            CreateItem();
            SetItem(_PhotoService, NewImage);
        }

        /// <summary>
        /// Wywołanie akcji użytkownika aplikacji
        /// </summary>
        /// <returns></returns>
        public async Task Invoke()
        {
            UserDialogs.Instance.ActionSheet(ActionSheetConfig);
        }

        /// <summary>
        /// Tworzenie elementów
        /// </summary>
        private void CreateItem()
        {
            ListActionSheetOptions = new List<ActionSheetOption>();
            ActionSheetConfig = new ActionSheetConfig();
        }

        /// <summary>
        /// Ustawienie elementów
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        private void SetItem(PhotoService _PhotoService, Action NewImage)
        {
            GenerateListActions(_PhotoService, NewImage);
            SetActionSheetConfig();
        }

        /// <summary>
        /// Tworzenie listy akcji użytkownika
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        private void GenerateListActions(PhotoService _PhotoService, Action NewImage)
        {
            ListActionSheetOptions.Add(ActionSheetTakeImage(_PhotoService, NewImage));
            ListActionSheetOptions.Add(ActionSheetPickImage(_PhotoService, NewImage));
            ListActionSheetOptions.Add(ActionSheetCancel());
        }

        /// <summary>
        /// Ustawienie konfiguracji dla 
        /// </summary>
        private void SetActionSheetConfig()
        {
            ActionSheetConfig.Options = ListActionSheetOptions;
            ActionSheetConfig.Title = "Zdjęcie";
        }


        /// <summary>
        /// Akcja wykonania zdjęcia
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <returns></returns>
        private ActionSheetOption ActionSheetTakeImage(PhotoService _PhotoService, Action NewImage)
        {
            string text = "Zrób zdjęcie";
            var ActionSheet = new ActionSheetOption(text, async () =>
            {
                var StatusTakePhoto = await TakePhoto(_PhotoService);
                
                if(StatusTakePhoto)
                {
                    NewImage.Invoke();
                }
                else
                {
                    UserDialogs.Instance.Alert("Nie udało się zrobić zdjęcia.");
                }

            });
            return ActionSheet;
        }
        /// <summary>
        /// Metoda pomocnicza do wykonania zdjęcia w zależności od konfiguracji modyfikatora wielkości
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <returns></returns>
        private async Task<bool> TakePhoto(PhotoService _PhotoService)
        {
            switch (_PhotoSizeType)
            {
                case PhotoSize.MaxWidthHeight:
                    {
                        var status = await _PhotoService.TakePhoto(_CompressQuality, _PhotoSize, PhotoSize.MaxWidthHeight);
                        return status;
                    }
                case PhotoSize.Custom:
                    {
                        var status = await _PhotoService.TakePhoto(_CompressQuality, _PhotoSize, PhotoSize.Custom);
                        return status;
                    }
                default:
                    {
                        return false;
                    }
            }
        }

        /// <summary>
        /// Akcja pobrania zdjęcia z pamięci urządzenia
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <returns></returns>
        private ActionSheetOption ActionSheetPickImage(PhotoService _PhotoService, Action NewImage)
        {
            string text = "Wybierz zdjęcie";
            var ActionSheet = new ActionSheetOption(text, async () =>
            {
                var StatusPickPhoto = await PickPhoto(_PhotoService);
                
                if (StatusPickPhoto)
                {
                    NewImage.Invoke();
                }
                else
                {
                    UserDialogs.Instance.Alert("Nie udało się wybrać zdjęcia.");
                }
            });
            return ActionSheet;
        }
        /// <summary>
        /// Metoda pomocnicza do pobrania zdjęcia z urządzenia w zależności od konfiguracji modyfikatora wielkości
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <returns></returns>
        private async Task<bool> PickPhoto(PhotoService _PhotoService)
        {
            switch (_PhotoSizeType)
            {
                case PhotoSize.MaxWidthHeight:
                    {
                        var status = await _PhotoService.PickPhoto(_CompressQuality, _PhotoSize, PhotoSize.MaxWidthHeight);
                        return status;
                    }
                case PhotoSize.Custom:
                    {
                        var status = await _PhotoService.PickPhoto(_CompressQuality, _PhotoSize, PhotoSize.Custom);
                        return status;
                    }
                default:
                    {
                        return false;
                    }
            }
        }
        /// <summary>
        /// Akcja anulowania
        /// </summary>
        /// <returns></returns>
        private ActionSheetOption ActionSheetCancel()
        {
            string text = "Anuluj";
            var ActionSheet = new ActionSheetOption(text, async () =>
            {
                //Do Nothing
            });
            return ActionSheet;
        }
    }
}
