﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using XamarinManiaLib.PluginMedia.Services;

namespace XamarinManiaLib.PluginMedia.UserActions
{
    internal class ImageActions
    {
        private List<ActionSheetOption> ListActionSheetOptions;
        private ActionSheetConfig ActionSheetConfig;
        private int _CompressQuality, _PhotoSize;
        private PhotoSize _PhotoSizeType;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <param name="DeleteImage">Akcja modyfikująca widok po usunięciu zdjęcia</param>
        /// <param name="PercentCompresion">Kompresja zdjęcia: '0' - Maksymalna kompresja, najgorsza jakość / '100' - Brak kompresji, najlepsza jakość</param>
        /// <param name="PercentPhotoSize">Wielkość zdjęciaj, '100' - Maksymalna wielkość / '99', '98' - Wielkość pomniejszana procentowo względem maksymalnej wielkości.
        public ImageActions(PhotoService _PhotoService, Action NewImage, Action DeleteImage, Int16 PercentCompresion, Int16 PercentPhotoSize)
        {
            _CompressQuality = PercentCompresion;
            _PhotoSize = PercentPhotoSize;
            _PhotoSizeType = PhotoSize.Custom;
            
            CreateItem();
            SetItem(_PhotoService, NewImage, DeleteImage);
        }
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <param name="DeleteImage">Akcja modyfikująca widok po usunięciu zdjęcia</param>
        /// <param name="PercentCompresion">Kompresja zdjęcia: '0' - Maksymalna kompresja, najgorsza jakość / '100' - Brak kompresji, najlepsza jakość</param>
        /// <param name="PhotoMaxWidthHeight">Maksymalna szerokość i wysokość zdjęcia</param>
        public ImageActions(PhotoService _PhotoService, Action NewImage, Action DeleteImage, Int32 PercentCompresion, Int32 PhotoMaxWidthHeight)
        {
            _CompressQuality = PercentCompresion;
            _PhotoSize = PhotoMaxWidthHeight;
            _PhotoSizeType = PhotoSize.MaxWidthHeight;

            CreateItem();
            SetItem(_PhotoService, NewImage, DeleteImage);
        }
        /// <summary>
        /// Wywołanie akcji użytkownika aplikacji
        /// </summary>
        /// <returns></returns>
        public async Task Invoke()
        {
            UserDialogs.Instance.ActionSheet(ActionSheetConfig);
        }

        /// <summary>
        /// Tworzenie elementów
        /// </summary>
        private void CreateItem()
        {
            ListActionSheetOptions = new List<ActionSheetOption>();
            ActionSheetConfig = new ActionSheetConfig();
        }

        /// <summary>
        /// Ustawienie elementów
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <param name="DeleteImage">Akcja modyfikująca widok po usunięciu zdjęcia</param>
        private void SetItem(PhotoService _PhotoService, Action NewImage, Action DeleteImage)
        {
            GenerateListActions(_PhotoService, NewImage, DeleteImage);
            SetActionSheetConfig();
        }

        /// <summary>
        /// Tworzenie listy akcji użytkownika
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <param name="DeleteImage">Akcja modyfikująca widok po usunięciu zdjęcia</param>
        private void GenerateListActions(PhotoService _PhotoService, Action NewImage, Action DeleteImage)
        {
            ListActionSheetOptions.Add(ActionSheetDeleteAndTakeImage(_PhotoService, NewImage, DeleteImage));
            ListActionSheetOptions.Add(ActionSheetDeleteAndPickImage(_PhotoService, NewImage, DeleteImage));
            ListActionSheetOptions.Add(ActionSheetDeleteImage(_PhotoService, DeleteImage));
            ListActionSheetOptions.Add(ActionSheetCancel());
        }

        /// <summary>
        /// Ustawienie konfiguracji dla 
        /// </summary>
        private void SetActionSheetConfig()
        {
            ActionSheetConfig.Options = ListActionSheetOptions;
            ActionSheetConfig.Title = "Zdjęcie";
        }

        /// <summary>
        /// Akcja usunięcia zdjęcia i wykonania następnego
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <param name="DeleteImage">Akcja modyfikująca widok po usunięciu zdjęcia</param>
        /// <returns>ActionSheetOption</returns>
        private ActionSheetOption ActionSheetDeleteAndTakeImage(PhotoService _PhotoService, Action NewImage, Action DeleteImage)
        {
            string text = "Zrób zdjęcie ponownie";
            var ActionSheet = new ActionSheetOption(text, async () =>
            {
                if (Device.RuntimePlatform == Device.UWP)
                {
                    _PhotoService.IsPhotoExistChangeManual(false);
                    var StatusTakePhoto = await TakePhoto(_PhotoService);

                    if (StatusTakePhoto)
                    {
                        NewImage.Invoke();
                    }
                    else
                    {
                        UserDialogs.Instance.Alert("Nie wykonano ponownego zdjęcia");
                    }
                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    var StatusDeletePhoto = await _PhotoService.DeletePhoto();
                    if (StatusDeletePhoto || !_PhotoService.IsPhotoExist())
                    {
                        var StatusTakePhoto = await TakePhoto(_PhotoService);

                        if (StatusTakePhoto)
                        {
                            NewImage.Invoke();
                        }
                        else
                        {
                            UserDialogs.Instance.Alert("Nie wykonano ponownego zdjęcia");
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.Alert("Wystąpił błąd podczas ponownego robienia zdjęcia");
                    }
                }
                else if (Device.RuntimePlatform == Device.iOS)
                {
                    throw new Exception("ActionSheetDeleteAndTakeImage() = Code not implemented");
                }
            });
            return ActionSheet;
        }

        /// <summary>
        /// Metoda pomocnicza do wykonania zdjęcia w zależności od konfiguracji modyfikatora wielkości
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <returns></returns>
        private async Task<bool> TakePhoto(PhotoService _PhotoService)
        {
            switch (_PhotoSizeType)
            {
                case PhotoSize.MaxWidthHeight:
                    {
                        var status = await _PhotoService.TakePhoto(_CompressQuality, _PhotoSize, PhotoSize.MaxWidthHeight);
                        return status;
                    }
                case PhotoSize.Custom:
                    {
                        var status = await _PhotoService.TakePhoto(_CompressQuality, _PhotoSize, PhotoSize.Custom);
                        return status;
                    }
                default:
                    {
                        return false;
                    }
            }
        }

        /// <summary>
        /// Akcja usunięcia zdjęcia i pobrania następnego z pamięci urządzenia
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="NewImage">Akcja modyfikująca widok po nowym zdjęciu</param>
        /// <param name="DeleteImage">Akcja modyfikująca widok po usunięciu zdjęcia</param>
        /// <returns>ActionSheetOption</returns>
        private ActionSheetOption ActionSheetDeleteAndPickImage(PhotoService _PhotoService, Action NewImage, Action DeleteImage)
        {
            string text = "Wybierz zdjęcie ponownie";
            var ActionSheet = new ActionSheetOption(text, async () =>
            {
                if (Device.RuntimePlatform == Device.UWP)
                {
                    _PhotoService.IsPhotoExistChangeManual(false);
                    var StatusPickPhoto = await PickPhoto(_PhotoService);
                    
                    if (StatusPickPhoto)
                    {
                        NewImage.Invoke();
                    }
                    else
                    {
                        UserDialogs.Instance.Alert("Nie wybrano zdjęcia");
                    }
                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    var StatusDeletePhoto = await _PhotoService.DeletePhoto();
                    if (StatusDeletePhoto || !_PhotoService.IsPhotoExist())
                    {
                        var StatusPickPhoto = await PickPhoto(_PhotoService);
                        
                        if (StatusPickPhoto)
                        {
                            NewImage.Invoke();
                        }
                        else
                        {
                            UserDialogs.Instance.Alert("Nie wybrano zdjęcia");
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.Alert("Wystąpił błąd podczas ponownego wybierania zdjęcia");
                    }
                }
                else if (Device.RuntimePlatform == Device.iOS)
                {
                    //No code yet
                }
            });
            return ActionSheet;
        }
        /// <summary>
        /// Metoda pomocnicza do pobrania zdjęcia z urządzenia w zależności od konfiguracji modyfikatora wielkości
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <returns></returns>
        private async Task<bool> PickPhoto(PhotoService _PhotoService)
        {
            switch (_PhotoSizeType)
            {
                case PhotoSize.MaxWidthHeight:
                    {
                        var status = await _PhotoService.PickPhoto(_CompressQuality, _PhotoSize, PhotoSize.MaxWidthHeight);
                        return status;
                    }
                case PhotoSize.Custom:
                    {
                        var status = await _PhotoService.PickPhoto(_CompressQuality, _PhotoSize, PhotoSize.Custom);
                        return status;
                    }
                default:
                    {
                        return false;
                    }
            }
        }
        /// <summary>
        /// Akcja usunięcia zdjęcia
        /// </summary>
        /// <param name="_PhotoService">Serwis obsługujący plugin media</param>
        /// <param name="DeleteImage">Akcja modyfikująca widok po usunięciu zdjęcia</param>
        /// <returns></returns>
        private ActionSheetOption ActionSheetDeleteImage(PhotoService _PhotoService, Action DeleteImage)
        {
            string text = "Usuń zdjęcie";
            var ActionSheet = new ActionSheetOption(text, async () =>
            {
                if(Device.RuntimePlatform == Device.UWP)
                {
                    _PhotoService.IsPhotoExistChangeManual(false);
                    DeleteImage.Invoke();
                }
                else if(Device.RuntimePlatform == Device.Android)
                {
                    var StatusDeletePhoto = await _PhotoService.DeletePhoto();
                    if (StatusDeletePhoto || !_PhotoService.IsPhotoExist())
                    {
                        DeleteImage.Invoke();
                    }
                    else
                    {
                        UserDialogs.Instance.Alert("Wystąpił błąd podczas usuwania zdjęcia");
                    }
                }
                else if(Device.RuntimePlatform == Device.iOS)
                {
                    //No code yet
                }
            });
            return ActionSheet;
        }

        /// <summary>
        /// Akcja anulowania 
        /// </summary>
        /// <returns></returns>
        private ActionSheetOption ActionSheetCancel()
        {
            string text = "Anuluj";
            var ActionSheet = new ActionSheetOption(text, async () =>
            {
               //Do Nothing
            });
            return ActionSheet;
        }
    }
}
